package az.ingress.service.announcement;

import az.ingress.config.SecurityConfig;
import az.ingress.dto.announcement.AnnouncementRequest;
import az.ingress.dto.announcement.AnnouncementResponse;
import az.ingress.dto.announcementdetail.AnnouncementDetailRequestDto;
import az.ingress.dto.announcementdetail.AnnouncementDetailResponseDto;
import az.ingress.entity.Announcement;
import az.ingress.entity.AnnouncementDetail;
import az.ingress.entity.User;
import az.ingress.exception.type.BaseException;
import az.ingress.mapper.AnnouncementMapper;
import az.ingress.mapper.PageResponseMapper;
import az.ingress.repository.AnnouncementDetailRepository;
import az.ingress.repository.AnnouncementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AnnouncementServiceImplTest {

    @Mock
    private AnnouncementRepository announcementRepository;
    @Mock
    private AnnouncementDetailRepository announcementDetailRepository;
    @Mock
    private SecurityConfig securityConfig;
    @Mock
    private AnnouncementMapper announcementMapper;
    @Mock
    private PageResponseMapper pageResponseMapper;

    @InjectMocks
    private AnnouncementServiceImpl announcementService;

    private Announcement announcement;
    private AnnouncementResponse announcementResponse;
    private User loggedInUser;
    private AnnouncementRequest announcementRequest;

    @BeforeEach
    void setUp() {
        loggedInUser = User.builder()
                .id(1L)
                .username("asiman")
                .build();

        announcement = Announcement.builder()
                .id(1L)
                .announcementDetail(
                        AnnouncementDetail.builder()
                                .description("Mercedes w202 C200")
                                .price("10000 AZN")
                                .title("w202")
                                .build()
                )
                .user(loggedInUser)
                .viewCount(0L)
                .build();

        announcementResponse = AnnouncementResponse.builder()
                .id(1L)
                .announcementDetail(
                        AnnouncementDetailResponseDto.builder()
                                .description("Mercedes w202 C200")
                                .price("10000 AZN")
                                .title("w202")
                                .build()
                )
                .viewCount(0L)
                .build();

        announcementRequest = AnnouncementRequest.builder()
                .announcementDetail(
                        AnnouncementDetailRequestDto.builder()
                                .description("Mercedes w202 C200")
                                .price("10000 AZN")
                                .title("w202")
                                .build()
                )
                .build();
    }

    @Test
    void givenValidAnnouncementRequest_whenCreateAnnouncement_thenAnnouncementIsCreatedSuccessfully() {
        // act , arrange, assert

        when(securityConfig.getLoggedInUser()).thenReturn(loggedInUser);

        when(announcementMapper.mapAnnouncementRequestToEntity(announcementRequest)).thenReturn(announcement);
        when(announcementRepository.save(announcement)).thenReturn(announcement);
        when(announcementMapper.mapAnnouncementEntityToResponse(announcement)).thenReturn(announcementResponse);

        assertEquals(announcementResponse, announcementService.createAnnouncement(announcementRequest));

    }

    @Test
    void givenValidAnnouncementId_whenDeleteAnnouncement_thenAnnouncementIsDeletedSuccessfully() {
        // act , arrange, assert

        when(announcementRepository.findById(1L)).thenReturn(Optional.of(announcement));
        when(announcementMapper.mapAnnouncementEntityToResponse(announcement)).thenReturn(announcementResponse);

        assertEquals(announcementResponse, announcementService.deleteAnnouncement(1L));

    }

    @Test
    void givenInvalidAnnouncementId_whenDeleteAnnouncement_thenThrowNotFoundException() {
        // act , arrange, assert

        when(announcementRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(BaseException.class, () -> announcementService.deleteAnnouncement(1L));

    }

    @Test
    void givenValidAnnouncementId_whenGetAnnouncementById_thenAnnouncementIsReturnedSuccessfully() {
        // act , arrange, assert

        when(announcementRepository.findById(1L)).thenReturn(Optional.of(announcement));
        when(announcementMapper.mapAnnouncementEntityToResponse(announcement)).thenReturn(announcementResponse);

        assertEquals(announcementResponse, announcementService.getAnnouncementById(1L));

    }

    @Test
    void testGetOwnMostViewedAnnouncement() {

        when(securityConfig.getLoggedInUser()).thenReturn(loggedInUser);
        when(announcementRepository.findTopByUserOrderByViewCountDesc(loggedInUser)).thenReturn(Optional.of(announcement));
        when(announcementMapper.mapAnnouncementEntityToResponse(announcement)).thenReturn(announcementResponse);


        assertEquals(announcementResponse, announcementService.getOwnMostViewedAnnouncement());
        verify(announcementRepository, times(1)).findTopByUserOrderByViewCountDesc(loggedInUser);
    }
}