package az.ingress.repository;

import az.ingress.entity.AnnouncementDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnnouncementDetailRepository extends JpaRepository<AnnouncementDetail, Long> {
}
