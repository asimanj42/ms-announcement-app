package az.ingress.config;

import az.ingress.filter.JwtAuthenticationFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class AnnouncementSecurityConfig extends SecurityConfig {

    private final String[] AUTH_WHITELIST = {
            "/api/announcements/search",
            "/api/announcements/most-viewed",
    };

    public AnnouncementSecurityConfig(JwtAuthenticationFilter jwtAuthenticationFilter, AuthenticationProvider authenticationProvider) {
        super(jwtAuthenticationFilter, authenticationProvider);
    }


    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(request -> request
                .requestMatchers(AUTH_WHITELIST).permitAll()
                .anyRequest().authenticated()
        );
        return super.securityFilterChain(http);
    }
}
