package az.ingress.service.auth;

import az.ingress.dto.auth.LoginRequest;
import az.ingress.dto.auth.LoginResponse;
import az.ingress.dto.auth.RegisterRequest;

public interface AuthService {
    LoginResponse login(LoginRequest loginRequest);

    void register(RegisterRequest registerRequest);

    void activate(String email, Integer verificationCode);

    void resendVerificationCode(String email);

}
