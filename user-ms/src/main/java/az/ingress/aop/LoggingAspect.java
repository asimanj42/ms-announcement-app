package az.ingress.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LoggingAspect {



    @Around("execution(* az.ingress.service.auth.AuthServiceImpl.*(..))")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        String methodName = joinPoint.getSignature().toShortString();

        log.info("Method {} started", methodName);

        Object result;
        try {
            result = joinPoint.proceed();
        } catch (Throwable throwable) {
            log.error("Method {} failed", methodName, throwable);
            throw throwable;
        }

        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;

        log.info("Method {} finished in {} ms", methodName, duration);

        return result;
    }

}